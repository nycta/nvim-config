vim.g['conjure#mapping#prefix'] = '<localleader>c'
vim.g['conjure#extract#tree_sitter#enabled'] = true
vim.g['conjure#extract#tree_sitter#enabled'] = true
vim.g['conjure#client#clojure#nrepl#connection#auto_repl#enabled'] = false
vim.g['conjure#filetype#scheme'] = 'conjure.client.guile.socket'
vim.g['conjure#client#guile#socket#pipename'] = ".guile-repl.socket"
-- vim.g['conjure#mapping#doc_word'] = false
