vim.o.hlsearch = false
vim.wo.number = true
vim.o.mouse = 'a'
vim.o.clipboard = 'unnamedplus'
vim.o.breakindent = true
vim.o.undofile = true
vim.o.ignorecase = true
vim.o.smartcase = true
vim.wo.signcolumn = 'yes'
vim.o.updatetime = 250
vim.o.timeout = true
vim.o.timeoutlen = 300
vim.o.completeopt = 'menuone,noselect'

vim.keymap.set({ 'n', 'v' }, '<Space>', '<Nop>', { silent = true })
vim.keymap.set('n', 'k', "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
vim.keymap.set('n', 'j', "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })
local highlight_group = vim.api.nvim_create_augroup('YankHighlight', { clear = true })
vim.api.nvim_create_autocmd('TextYankPost', {
  callback = function()
    vim.highlight.on_yank()
  end,
  group = highlight_group,
  pattern = '*',
})

vim.o.expandtab = true
vim.o.tabstop=4
vim.o.softtabstop=4
vim.o.shiftwidth=4

-- vim.api.nvim_create_autocmd({'FileType'}, {
--   pattern = 'svelte',
--   callback = function()
--     local buf = vim.api.nvim_get_current_buf()
--     vim.api.nvim_buf_set_option(buf, 'filetype', 'html')
--   end
-- })

vim.keymap.set('n', '<C-e>',  ':Texplore<CR>',    { silent = true })
vim.keymap.set('n', ' bd',    ':bd<CR>',          { silent = true, desc = '[B]uffer [D]elete' })
vim.keymap.set('n', ' bn',    ':bn<CR>',          { silent = true, desc = '[B]uffer [N]ext' })
vim.keymap.set('n', ' bp',    ':bp<CR>',          { silent = true, desc = '[B]uffer [P]rev' })
vim.keymap.set('n', ' bDAEC', ':%bd|e#<CR>',      { silent = true, desc = '[B]uffer [D]elete [A]ll [E]xcept [C]urrent' })

vim.keymap.set('n', ' ts',    ':tab split<CR>',   { silent = true, desc = 'Make tab last' })
vim.keymap.set('n', ' >',     ':tabmove<CR>',     { silent = true, desc = 'Make tab last' })
vim.keymap.set('n', ' <',     ':0tabmove<CR>',    { silent = true, desc = 'Make tab last' })
vim.keymap.set('n', ' .',     ':+tabmove<CR>',    { silent = true, desc = 'Make tab last' })
vim.keymap.set('n', ' ,',     ':-tabmove<CR>',    { silent = true, desc = 'Make tab last' })

-- vim: ts=2 sts=2 sw=2 et
