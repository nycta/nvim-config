vim.o.background = 'dark'
vim.o.termguicolors = false

local white = 15
local black = 0
local gray = 8
local bg_gray = 7
local green = 2
local br_green = 10
local pink = 5

vim.api.nvim_set_hl(0, 'TabLine',     { ctermbg=gray, ctermfg=white, bold=false })
vim.api.nvim_set_hl(0, 'TabLineSel',  { ctermbg=green, ctermfg=white, bold=true })

vim.api.nvim_set_hl(0, 'Visual',  { ctermbg=green, ctermfg=white })

vim.api.nvim_set_hl(0, 'Pmenu',     { ctermbg=gray,   ctermfg=white })
vim.api.nvim_set_hl(0, 'PmenuSel',  { ctermbg=green,  ctermfg=white })

vim.api.nvim_set_hl(0, 'NormalFloat',   { ctermbg=gray,   ctermfg=white })

vim.api.nvim_set_hl(0, 'WhichKey',            { ctermbg=gray,   ctermfg=br_green,   bold=true })
vim.api.nvim_set_hl(0, 'WhichKeyGroup',       { ctermbg=gray,   ctermfg=white })
vim.api.nvim_set_hl(0, 'WhichKeySeparator',   { ctermbg=gray,   ctermfg=br_gray })
vim.api.nvim_set_hl(0, 'WhichKeyDesc',        { ctermbg=gray,   ctermfg=white })
vim.api.nvim_set_hl(0, 'WhichKeyValue',       { ctermbg=gray,   ctermfg=white })
vim.api.nvim_set_hl(0, 'WhichKeyFloat',       { ctermbg=gray,   ctermfg=white })
-- vim.api.nvim_set_hl(0, 'WhichKeyBorder',    { ctermbg=black,   ctermfg=white  })

vim.api.nvim_set_hl(0, "Comment", { ctermfg = pink })

-- vim: ts=2 sts=2 sw=2 et
