
vim.api.nvim_create_autocmd({'FileType'}, {
  pattern = 'netrw',
  callback = function()
    vim.keymap.set('n', 'S', '<nop>', { buffer=true, remap=true })
  end
})
