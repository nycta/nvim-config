require('00_init')
require('10_opts')
require('10_netrw')
require('20_lazy')
require('30_setup')
require('30_setup_telescope')
require('30_setup_treesitter')
require('30_setup_parinfer')
require('30_setup_conjure')
require('30_setup_lsp')
require('90_colors')

-- vim: ts=2 sts=2 sw=2 et
